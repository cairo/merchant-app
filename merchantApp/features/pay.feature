Feature: merchant payment

	Scenario: Pay request success using token
		Given a merchant who is registered with DTUPay
		And the merchant has a bank account with a balance of 200
		And a customer who is registered with DTUPay
		And the customer has a bank account with a balance of 500
		And the customer provides a valid bar code
		When the merchant submits a request for payment of 100
		Then the transaction is successful

	Scenario: Pay request fail using token when payment amount surpasses the customer's balance
		Given a merchant who is registered with DTUPay
		And the merchant has a bank account with a balance of 200
		And a customer who is registered with DTUPay
		And the customer has a bank account with a balance of 500
		And the customer provides a valid bar code
		When the merchant submits a request for payment of 600
		Then the transaction fails

	Scenario: Pay request fail using token when submitting an invalid bar code
		Given a merchant who is registered with DTUPay
		And the merchant has a bank account with a balance of 200
		And a customer who is registered with DTUPay
		And the customer has a bank account with a balance of 500
		And the customer provides an invalid bar code
		When the merchant submits a request for payment of 100
		Then the transaction fails
		
	Scenario: Pay request fail when merchant is not registered in DTUPay
		Given a merchant who is not registered with DTUPay
		And the merchant has a bank account with a balance of 200
		And a customer who is registered with DTUPay
		And the customer has a bank account with a balance of 500
		And the customer provides a valid bar code
		When the merchant submits a request for payment of 100
		Then the transaction fails
		
	Scenario: Pay request fail when customer is not registered in DTUPay
		Given a merchant who is registered with DTUPay
		And the merchant has a bank account with a balance of 200
		And a customer who is not registered with DTUPay
		And the customer has a bank account with a balance of 500
		And the customer provides a valid bar code
		When the merchant submits a request for payment of 100
		Then the transaction fails
			

	Scenario: A merchant can be successfully created
		Given a merchant with cpr "0000000001" and first name: "Henning" and last name: "Frederiksen"
		And the merchant does not already exist in the DTUpay system
		When the merchant tries to register to DTUpay
		Then the merchant will be succesfully registered

	Scenario: A merchant can not be created twice
		Given a merchant with cpr "0000000001" and first name: "Henning" and last name: "Frederiksen"
		And the merchant exists in the DTUpay system
		When the merchant tries to register to DTUpay
		Then the merchant will not be registered again

	Scenario: A merchant can be successfully deleted
		Given a merchant with cpr "0000000001" and first name: "Henning" and last name: "Frederiksen"
		And the merchant exists in the DTUpay system
		When the merchant requests to be deleted
		Then the merchant does not exist in the database anymore

	Scenario: A merchant can not be deleted if they do not exist in the system
		Given a merchant with cpr "0000000001" and first name: "Henning" and last name: "Frederiksen"
		And the merchant does not already exist in the DTUpay system
		When the merchant requests to be deleted
		Then the merchant will be told that they are not in the system already