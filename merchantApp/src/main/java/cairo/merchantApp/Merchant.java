package cairo.merchantApp;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * @author Michael
 */
@XmlRootElement
public class Merchant {

    public Merchant(){
        account = "";
    }

    public String cpr;

    public String firstName;

    public String lastName;

    private String account;
}
