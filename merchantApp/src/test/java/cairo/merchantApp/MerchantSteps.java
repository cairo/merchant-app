package cairo.merchantApp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.ws.fastmoney.*;
import org.json.JSONArray;
import org.json.JSONObject;
import dtu.ws.fastmoney.User;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * cucumber steps used for integration testing the payment use case
 * @author Michael
 */

public class MerchantSteps {
	Client userClient = ClientBuilder.newClient();
	WebTarget webTargetUserEndpoint = userClient.target("http://02267-cairo.compute.dtu.dk:8383/usermanager");
	User merchant;
	User customer;
	Merchant restMerchant;
	Customer restCustomer;
	Account customerAcc;
	Account merchantAcc;
	BigDecimal originalCustomerBalance;
	BigDecimal originalMerchantBalance;
	BigDecimal transactionAmount;
	BankService bs = new BankServiceService().getBankServicePort();
	String tokenId;
	String tokenURL;
	Response paymentResponse;
	ArrayList<String> testUserCprs = new ArrayList<String>();

	@After
	public void databaseCleanup(){
		System.out.println("The database was wiped");
		for(String cpr: testUserCprs){
			System.out.println("Merchant with cpr: " + cpr + " was wiped");
			MerchantTestSupport.deleteUser(webTargetUserEndpoint, cpr);
		}
	}

	@Given("^a merchant who is registered with DTUPay$")
	public void aMerchantWhoIsRegisteredWithDTUPay() {
		boolean isCostumer = false;
		restMerchant = new Merchant();
		restMerchant.cpr = "9999999999";
		restMerchant.firstName = "Jonas";
		restMerchant.lastName = "Jensen";
		Response createUserResponse = MerchantTestSupport.createMerchant(webTargetUserEndpoint, restMerchant, testUserCprs);
		assertEquals("The user was added", createUserResponse.readEntity(String.class));
		assertEquals(200, createUserResponse.getStatus());
		String validateResponse = MerchantTestSupport.validate(webTargetUserEndpoint, restMerchant.cpr, isCostumer);
		assertEquals("User Found", validateResponse);
	}

	@Given("^a merchant who is not registered with DTUPay$")
	public void aMerchantWhoIsNotRegisteredWithDTUPay() {
		boolean isCostumer = false;
		restMerchant = new Merchant();
		restMerchant.cpr = "0000000000";
		restMerchant.firstName = "Jonas";
		restMerchant.lastName = "Jensen";
		String validateResponse = MerchantTestSupport.validate(webTargetUserEndpoint, restMerchant.cpr, isCostumer);
		assertEquals("User Not Found", validateResponse);
	}

	@Given("^the merchant has a bank account with a balance of (\\d+)$")
	public void theMerchantHasABankAccountWithABalanceOf(int arg1) throws Throwable {
		System.out.println("start: the merchant has a bank account with a balance of");
		merchant = new User();
		String cpr = restMerchant.cpr;
		merchant.setCprNumber(cpr);
		merchant.setFirstName("John");
		merchant.setLastName("Smokey");
		String acc = "";
		try{
			merchantAcc = bs.getAccountByCprNumber(cpr);
			bs.retireAccount(merchantAcc.getId());
		}catch(Exception e){		}
		try {
			acc = bs.createAccountWithBalance(merchant, new BigDecimal(arg1));
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
		}

		merchantAcc = bs.getAccount(acc);
		assertEquals(new BigDecimal(arg1),merchantAcc.getBalance());
		originalMerchantBalance = new BigDecimal(arg1);
		System.out.println("stop: the merchant has a bank account with a balance of");
	}

	@Given("^a customer who is registered with DTUPay$")
	public void aCustomerWhoIsRegisteredWithDTUPay() {

		boolean isCostumer = true;
		restCustomer = new Customer();
		restCustomer.cpr = "1234567890";
		restCustomer.firstName = "Jonas";
		restCustomer.lastName = "Michaelsen";
		Response createUserResponse = MerchantTestSupport.createCustomer(webTargetUserEndpoint, restCustomer, testUserCprs);
		assertEquals("The user was added", createUserResponse.readEntity(String.class));
		assertEquals(200, createUserResponse.getStatus());
		String validateResponse = MerchantTestSupport.validate(webTargetUserEndpoint, restCustomer.cpr, isCostumer);
		assertEquals("User Found", validateResponse);
	}

	@Given("^a customer who is not registered with DTUPay$")
	public void aCustomerWhoIsNotRegisteredWithDTUPay() {

		boolean isCostumer = true;
		restCustomer = new Customer();
		restCustomer.cpr = "0987654321";
		restCustomer.firstName = "Jonas";
		restCustomer.lastName = "Michaelsen";
		String validateResponse = MerchantTestSupport.validate(webTargetUserEndpoint, restCustomer.cpr, isCostumer);
		assertEquals("User Not Found", validateResponse);
	}

	@Given("^the customer has a bank account with a balance of (\\d+)$")
	public void theCustomerHasABankAccountWithABalanceOf(int arg1) throws Throwable {
		System.out.println("Start: the customer has a bank account with a balance of");
		customer = new User();
		String cpr = restCustomer.cpr;
		customer.setCprNumber(cpr);
		customer.setFirstName("Jane");
		customer.setLastName("Doe");
		String acc = "";
		try{
			customerAcc = bs.getAccountByCprNumber(cpr);
			bs.retireAccount(customerAcc.getId());
		}catch(Exception e){		}
		try {
			acc = bs.createAccountWithBalance(customer, new BigDecimal(arg1));
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
		}

		customerAcc = bs.getAccount(acc);
		assertEquals(new BigDecimal(arg1),customerAcc.getBalance());
		originalCustomerBalance = new BigDecimal(arg1);
		System.out.println("End: the customer has a bank account with a balance of");
	}

	@Given("^the customer provides a valid bar code$")
	public void theCustomerProvidesAValidBarCode() {
		System.out.println("Start: the customer provides a valid bar code");
		String amount = "1";
		RestUser user = new RestUser();
		user.username= customer.getFirstName()+customer.getLastName();
		user.userID = customer.getCprNumber();

		Client client = ClientBuilder.newClient();
		WebTarget webtarget = client.target("http://02267-cairo.compute.dtu.dk:8080/tokenmanager");
		WebTarget webtargetToken = webtarget.queryParam("amount",amount);
		Invocation.Builder invocationBuilder = webtargetToken.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(user,MediaType.APPLICATION_JSON));
		assertEquals(200,response.getStatus());
		String output = response.readEntity(String.class);
		JSONArray resp = new JSONArray(output);
		JSONObject token = (JSONObject) resp.get(0);
		tokenId= token.getString("id");
		tokenURL = token.getString("url");

		assertTrue(tokenId!=null);
		assertTrue(tokenId!="");
		assertTrue(tokenURL!=null);
		assertTrue(tokenURL!="");
		System.out.println("End: the customer provides a valid bar code");
	}

	@And("^the customer provides an invalid bar code$")
	public void theCustomerProvidesAnInvalidBarCode() {
		System.out.println("Start: the customer provides an invalid bar code");
		tokenId= "some invalid token";
		tokenURL = "the url of some invalid token";
		System.out.println("End: the customer provides an invalid bar code");

	}


	@XmlRootElement
	public class RestUser {
		public String userID;
		public String username;

		public RestUser(){
		}
	}

	@When("^the merchant submits a request for payment of (\\d+)$")
	public void theMerchantSubmitsARequestForPaymentOfA(int arg1) {
		System.out.println("Start: the merchant submits a request for payment of");
		RestPayment paymentObj = new RestPayment();
		transactionAmount = new BigDecimal(arg1);
		paymentObj.amount = arg1;
		paymentObj.merchantId = merchant.getCprNumber();
		paymentObj.tokenId = tokenId;

		Client client = ClientBuilder.newClient();
		WebTarget webtarget = client.target("http://02267-cairo.compute.dtu.dk:8181/payment");
		Invocation.Builder invocationBuilder = webtarget.request(MediaType.TEXT_PLAIN);
		Response response = invocationBuilder.post(Entity.entity(paymentObj, MediaType.APPLICATION_JSON));
		paymentResponse = response;
		System.out.println("End: the merchant submits a request for payment of");
	}

	@XmlRootElement
	public class RestPayment {
		public String merchantId;
		public String tokenId;
		public int amount;

		public RestPayment(){
		}
	}

	@Then("^the transaction is successful$")
	public void thePaymentIsSuccessful() throws Throwable{
		System.out.println("Start: the payment is successful");
		Thread.sleep(5000);
		
		// Transaction successful
		assertEquals(200,paymentResponse.getStatus());
		String output = paymentResponse.readEntity(String.class);
		assertEquals("Payment successful",output);
		
		// Accounts have updated balance		
		customerAcc = bs.getAccountByCprNumber(customer.getCprNumber());
		BigDecimal balance = customerAcc.getBalance();
		assertEquals(originalCustomerBalance.subtract(transactionAmount),balance);
		
		merchantAcc = bs.getAccountByCprNumber(merchant.getCprNumber());
		balance = merchantAcc.getBalance();
		assertEquals(originalMerchantBalance.add(transactionAmount),balance);
		
		// Cleanup
		try{
			bs.retireAccount(customerAcc.getId());
			bs.retireAccount(merchantAcc.getId());
		}catch(Exception e){		}
		System.out.println("End: the payment is successful");
	}
	
	@Then("^the transaction fails$")
	public void theTransactionFails() throws Throwable {
		System.out.println("Start: the payment is successful");
		Thread.sleep(5000);
		
		// Account balances have not been updated		
		customerAcc = bs.getAccountByCprNumber(customer.getCprNumber());
		BigDecimal balance = customerAcc.getBalance();
		assertEquals(originalCustomerBalance,balance);
		
		merchantAcc = bs.getAccountByCprNumber(merchant.getCprNumber());
		balance = merchantAcc.getBalance();
		assertEquals(originalMerchantBalance,balance);
		
		// Cleanup
		try{
			bs.retireAccount(customerAcc.getId());
			bs.retireAccount(merchantAcc.getId());
		}catch(Exception e){		}
		System.out.println("End: the payment is successful");
	}
}

