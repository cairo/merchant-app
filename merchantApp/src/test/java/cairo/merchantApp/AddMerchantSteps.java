package cairo.merchantApp;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;

/**
 * @author Jonas
 * Steps for cucumbertest for creating/deleting merchants and customers
 */

public class AddMerchantSteps {

    Merchant merchant = new Merchant();
    Client client = ClientBuilder.newClient();
    WebTarget webtarget = client.target("http://02267-cairo.compute.dtu.dk:8383/usermanager");
    Response createUserResponse;
    Response deleteUserResponse;
    ArrayList<String> testUserCprs = new ArrayList<String>();

    @After
    public void databaseCleanup(){
        for(String cpr: testUserCprs){
            MerchantTestSupport.deleteUser(webtarget, cpr);
        }
    }

    @Given("^a merchant with cpr \"([^\"]*)\" and first name: \"([^\"]*)\" and last name: \"([^\"]*)\"$")
    public void aMerchantWithCprAndFirstNameAndLastName(String arg0, String arg1, String arg2) throws Throwable {
        merchant.cpr = arg0;
        merchant.firstName = arg1;
        merchant.lastName = arg2;
    }

    @And("^the merchant does not already exist in the DTUpay system$")
    public void theMerchantDoesNotAlreadyExistInTheDTUpaySystem() throws Throwable {
        boolean isCustomer = false;
        String returned = MerchantTestSupport.validate(webtarget, merchant.cpr, isCustomer);
        assertEquals("User Not Found", returned);
    }

    @When("^the merchant tries to register to DTUpay$")
    public void theMerchantTriesToRegisterToDTUpay() throws Throwable {
        createUserResponse = MerchantTestSupport.createMerchant(webtarget, merchant, testUserCprs);
    }

    @Then("^the merchant will be succesfully registered$")
    public void theMerchantWillBeSuccesfullyRegistered() throws Throwable {
        boolean isCustomer = false;
        assertEquals("The user was added", createUserResponse.readEntity(String.class));
        assertEquals(200, createUserResponse.getStatus());
        String returned = MerchantTestSupport.validate(webtarget, merchant.cpr, isCustomer);
        assertEquals("User Found", returned);
    }

    @And("^the merchant exists in the DTUpay system$")
    public void theMerchantExistsInTheDTUpaySystem() throws Throwable {
        Response status = MerchantTestSupport.createMerchant(webtarget, merchant, testUserCprs);
        assertEquals(200, status.getStatus());
    }

    @Then("^the merchant will not be registered again$")
    public void theMerchantWillNotBeRegisteredAgain() throws Throwable {
        boolean isCustomer = false;
        assertEquals("The user already exists", createUserResponse.getEntityTag().getValue());
        assertEquals(304, createUserResponse.getStatus());
        String returned = MerchantTestSupport.validate(webtarget, merchant.cpr, isCustomer);
        assertEquals("User Found", returned);
    }

    @When("^the merchant requests to be deleted$")
    public void theMerchantRequestsToBeDeleted() throws Throwable {
        deleteUserResponse = MerchantTestSupport.deleteUser(webtarget, merchant.cpr);
    }

    @Then("^the merchant does not exist in the database anymore$")
    public void theMerchantDoesNotExistInTheDatabaseAnymore() throws Throwable {
        boolean isCustomer = false;
        assertEquals(200, deleteUserResponse.getStatus());
        assertEquals("The user was deleted", deleteUserResponse.readEntity(String.class));
        String returned = MerchantTestSupport.validate(webtarget, merchant.cpr, isCustomer);
        assertEquals("User Not Found", returned);
    }

    @Then("^the merchant will be told that they are not in the system already$")
    public void theMerchantWillBeToldThatTheyAreNotInTheSystemAlready() throws Throwable {
        boolean isCustomer = false;
        assertEquals(304, deleteUserResponse.getStatus());
        assertEquals("The user was not found", deleteUserResponse.getEntityTag().getValue());
        String returned = MerchantTestSupport.validate(webtarget, merchant.cpr, isCustomer);
        assertEquals("User Not Found", returned);
    }

}
