package cairo.merchantApp;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

/**
 * @author Henning
 *
 * This class implements supportive functions, which are useful
 * for the cucumber tests in both "Steps" classes. They have been
 * extracted to a separate class, to improve code readability.
 */
public class MerchantTestSupport {

    /**
     * This function accesses the user microservice, and validates
     * whether a given user exists and whether they are a customer or merchant.
     *
     * @param WebTarget webTarget
     * @param String cpr
     * @param boolean isCustomer
     * @return
     */
    public static String validate(WebTarget webTarget, String cpr, boolean isCustomer){
        WebTarget webTargetFinal = webTarget.path("/" + cpr).queryParam("iscustomer", isCustomer);
        Invocation.Builder invocationBuilder = webTargetFinal.request(MediaType.TEXT_PLAIN);
        Response response= invocationBuilder.get();
        return response.readEntity(String.class);
    }

    /**
     * This function accesses the user microservice, and creates a merchant
     * in the user database.
     * @param WebTarget webTarget
     * @param Merchant user
     * @param ArrayList<String> testUserCprs
     * @return
     */
    public static Response createMerchant(WebTarget webTarget, Merchant user, ArrayList<String> testUserCprs){
        boolean isCustomer = false;
        WebTarget webTargetUser = webTarget.queryParam("iscustomer", isCustomer);
        Invocation.Builder invocationBuilder = webTargetUser.request(MediaType.TEXT_PLAIN);
        if(!testUserCprs.contains(user.cpr)){
            testUserCprs.add(user.cpr);
        }
        return invocationBuilder.post(Entity.entity(user, MediaType.APPLICATION_JSON));
    }

    /**
     * This function accesses the user microservice, and creates a customer
     * in the user database.
     * @param WebTarget webTarget
     * @param Customer user
     * @param ArrayList<String> testUserCprs
     * @return
     */
    public static Response createCustomer(WebTarget webTarget, Customer user, ArrayList<String> testUserCprs){
        boolean isCustomer = true;
        WebTarget webTargetUser = webTarget.queryParam("iscustomer", isCustomer);
        Invocation.Builder invocationBuilder = webTargetUser.request(MediaType.TEXT_PLAIN);
        if(!testUserCprs.contains(user.cpr)){
            testUserCprs.add(user.cpr);
        }
        return invocationBuilder.post(Entity.entity(user, MediaType.APPLICATION_JSON));
    }

    /**
     * This function accesses the user microservice, and deletes a user
     * from the user database. Deletion is based on cpr-number.
     * @param WebTarget webTarget
     * @param String cpr
     * @return
     */
    public static Response deleteUser(WebTarget webTarget, String cpr){
        WebTarget webTargetUser = webTarget.path("/" + cpr);
        Invocation.Builder invocationBuilder = webTargetUser.request(MediaType.TEXT_PLAIN);
        return invocationBuilder.delete();
    }
}
