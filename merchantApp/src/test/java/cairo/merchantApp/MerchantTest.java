package cairo.merchantApp;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

/**
 * glue class that specifies the feature file containing the scenarios to be run
 * @author Michael
 */

@RunWith(Cucumber.class)
@CucumberOptions(features="features", 
				 snippets=SnippetType.CAMELCASE)
public class MerchantTest {
}